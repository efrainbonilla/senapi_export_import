<?php

namespace Drupal\senapi_export_import;


use Drupal\Component\Render\PlainTextOutput;
use Drupal\Component\Utility\Html;
use Drupal\Core\Entity\EntityFieldManager;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Entity\Query\QueryFactory;
use Drupal\Core\File\FileSystem;
use Drupal\Core\Utility\Token;
use Drupal\node\Entity\Node;
use Drupal\node\Entity\NodeType;
use Drupal\menu_link_content\Entity\MenuLinkContent;
use Drupal\taxonomy\Entity\Term;
use Drupal\taxonomy\Entity\Vocabulary;
use Symfony\Component\HttpKernel\Exception\HttpException;

class EntitySaveHelper {
  /**
   * Drupal File System.
   *
   * @var \Drupal\Core\File\FileSystem
   */
  protected $entityTypeManager;

  /**
   * Drupal File System.
   *
   * @var \Drupal\Core\File\FileSystem
   */
  protected $fileSystem;

  /**
   * Drupal\Core\Entity\Query\QueryFactory definition.
   *
   * @var Drupal\Core\Entity\Query\QueryFactory
   */
  protected $entityQuery;

  /**
   * Drupal\Core\Entity\EntityFieldManager definition.
   *
   * @var Drupal\Core\Entity\Query\QueryFactory
   */
  protected $fieldManager;

  /**
   * @var array
   */
  public $folders;

  /**
   * @var \Drupal\Core\Utility\Token
   */
  protected $token;
  /**
   * Constructor for \Drupal\image_export_import\EntitySaveHelper class.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The Entity type manager.
   * @param \Drupal\Core\File\FileSystem $file_system
   *   The Form Builder.
   * @param \Drupal\Core\Entity\Query\QueryFactory $entityQuery
   *   The Query Builder.
   * @param \Drupal\Core\Entity\EntityFieldManager $fieldManager
   *   The Field manager.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, FileSystem $file_system, QueryFactory $entityQuery, EntityFieldManager $fieldManager, Token $token) {
    $this->entityTypeManager = $entity_type_manager;
    $this->fileSystem = $file_system;
    $this->entityQuery = $entityQuery;
    $this->fieldManager = $fieldManager;
    $this->folders = $this->createFolderExportImport();
    $this->token = $token;
  }

  /**
   * Obtener lista de tipos de contenidos
   *
   * @return array
   */
  public function getAllContentTypes() {
    $contentTypes = NodeType::loadMultiple();

    $contentTypesList = [];
    foreach ($contentTypes as $contentType) {
      $contentTypesList[$contentType->id()] = $contentType->label();
    }
    return $contentTypesList;
  }

  /**
   * Retorna campos basados en tipo de contenido
   *
   * @param $bundle
   * @param $entity_type_id
   *
   * @return array
   */
  public function getAllItemFields($bundle, $entity_type_id) {
    $bundleFields = [];

    foreach ($this->fieldManager->getFieldDefinitions($entity_type_id, $bundle) as $field_name => $field_definition) {
      if (!empty($field_definition->getTargetBundle()) && in_array($field_definition->getType(), ['image', 'entity_reference', 'file']) ) {
        $bundleFields[$field_name] = $field_definition->getLabel() . ' - ' . $field_definition->getName() . "(".$field_definition->getType() . ")";
      } else {
        if (in_array($field_definition->getType(), ['datetime', 'list_string','text_long'])) {
          $bundleFields[$field_name] = $field_definition->getLabel() . ' - ' . $field_definition->getName() . "(".$field_definition->getType() . ")";
        } else {
          if (!in_array($field_name, [
            'uuid', 'uid', 'nid', 'vid', 'langcode','title', 'type', 'created', 'changed', 'promote', 'sticky', 'default_langcode', 'path', 'comment',
            'revision_log', 'revision_uid', 'revision_timestamp', 'revision_langcode', 'revision_default', 'revision_translation_affected'
            ])) {
            $bundleFields[$field_name] = $field_definition->getLabel() . ' - ' . $field_definition->getName() . "(".$field_definition->getType() . ")";
          }
        }
      }
    }

    return $bundleFields;
  }
  /**
   * To get all Content Type Fields.
   */
  public function getFields($contentType) {
    $fields = [];
    foreach (\Drupal::entityManager()
               ->getFieldDefinitions('node', $contentType) as $field_definition) {
      if (!empty($field_definition->getTargetBundle()) && in_array($field_definition->getType(), ['image', 'entity_reference', 'file'])) {
        $fields['name'][] = $field_definition->getName();
        $fields['properties'][] = [
          'name' => $field_definition->getName(),
          'type' => $field_definition->getType(),
          'setting' => $field_definition->getSettings()
        ];
      } else {
        if (in_array($field_definition->getType(), ['datetime', 'list_string','text_long'])) {
          $fields['name'][] = $field_definition->getName();
          $fields['properties'][] = [
            'name' => $field_definition->getName(),
            'type' => $field_definition->getType(),
            'setting' => $field_definition->getSettings()
          ];
        } else {
          $fields['name'][] = $field_definition->getName();
          $fields['properties'][] = [
            'name' => $field_definition->getName(),
            'type' => $field_definition->getType(),
            'setting' => $field_definition->getSettings()
          ];
        }
      }
    }
    return $fields;
  }

  public function createCsvFileExportData($filename, $content_types, $fields, $export_body) {

    $pathAbsolute = $this->folders['public_migrate']['absolute'] . "/$filename";
    $pathRelative = $this->folders['public_migrate']['relative'] . "/$filename";

    file_unmanaged_delete($pathAbsolute);

    $file = fopen($pathAbsolute, 'w');

    // Agregar fila de columnas
    $column = [];
    $column[0] = $content_types . '_nid';
    $column[1] = $content_types . '_title';
    $column[2] = $content_types . '_created';
    $column[3] = $content_types . '_created_datetime';

    if (count($fields) >= 1) {
      foreach ($fields as $field) {
        $column[] = $content_types . '_' .  $field;

        if (preg_grep("/^.+image+.$/i", [$field]) || preg_grep("/^.+image$/i", [$field])){
          $column[] = $content_types . '_' .  $field . '_alt';
          $column[] = $content_types . '_' .  $field . '_title';
        }
        if (preg_grep("/^.+file+.$/i", [$field]) || preg_grep("/^.+file/i", [$field])){
          $column[] = $content_types . '_' .  $field . '_description';
        }
      }
    }

    // Agregar fila de columnas
    if (!empty($export_body)) {
      $column[] = $content_types . '_body_summary';
      $column[] = $content_types . '_body_description';
    }
    fputcsv($file, $column);

    $folder = $this->folders['export_media']['absolute'] . "/$content_types";

    if (!is_dir($folder)) {
      mkdir($folder, 0777, TRUE);
      chmod($folder, 0777);
    }

    $nids = \Drupal::entityQuery('node')->condition('type', $content_types)->execute();
    $nodes = $this->entityTypeManager->getStorage('node')->loadMultiple($nids);

    foreach ($nodes as $node) {
      $row = [];
      $row[0] = $node->get('nid')->value;
      $row[1] = $node->get('title')->value;
      $row[2] = $node->get('created')->value;
      $row[3] = date('Y-m-d H:i:s', $node->get('created')->value);

      foreach ($fields as $field) {
        $item_index = 0;
        $img_title = $img_alt = $file_description = $basename = [];

        $type = $node->getTypedData()->getDataDefinition()->getPropertyDefinition($field)->getType();
        $setting = $node->getTypedData()->getDataDefinition()->getPropertyDefinition($field)->getSettings();
        //kint(['type' => $type, 'setting' => $setting, 'field' => $field, 'values' => $node->get($field)->getValue()]);
        switch ($type) {
          case 'image':
          case 'file':
            if (!empty($node->get($field)->target_id) && ($values = $node->get($field)->getValue()) && count($values) >= 1) {

              foreach ($node->get($field)->referencedEntities() as $item) {
                switch ($item->getEntityTypeId()) {
                  case 'file':
                    $data = $this->entityTypeManager->getStorage('file')
                      ->load($item->id());

                    $item_ = $values[$item_index];

                    if (!empty($data)) {
                      $basename[$item_index] = basename($data->getFileUri());
                      $src = $this->fileSystem->realpath($data->getFileUri());
                      $dest = $folder;

                      shell_exec("cp -r $src $dest");
                    }

                    switch ($type) {
                      case 'image':
                        $img_alt[$item_index] = ($item_['alt']) ? $item_['alt'] : '';
                        $img_title[$item_index] = ($item_['title']) ? $item_['title'] : '';
                        $item_index++;

                        if (count($values) == $item_index) {
                          $row[] = implode("|", $basename);
                          $row[] = implode("|", $img_alt);
                          $row[] = implode("|", $img_title);
                        }
                        break;

                      case 'file':
                        $file_description[$item_index] = ($item_['description'])? $item_['description'] : '';
                        $item_index++;
                        if (count($values) == $item_index) {
                          $row[] = implode("|", $basename);
                          $row[] = implode("|", $file_description);
                        }
                        break;
                    }

                    break;
                }
              }
            }  else {
              if (preg_grep("/^.+image+.$/i", [$field]) || preg_grep("/^.+image$/i", [$field])) {
                $row[] = "";
                $row[] = "";
                $row[] = "";
              } elseif (preg_grep("/^.+file$/i", [$field]) || preg_grep("/^.+file$/i", [$field])) {
                $row[] = "";
                $row[] = "";
              } else {
                $row[] = "";
              }
            }
            break;

          case 'entity_reference':
            if (!empty($node->get($field)->target_id) ) {
              $valuesReference = $node->get($field)->getValue();
              foreach ($node->get($field)->referencedEntities() as $item) {
                switch ($item->getEntityTypeId()) {
                  case 'taxonomy_term':
                    $data = $this->entityTypeManager->getStorage('taxonomy_term')
                      ->load($item->id());

                    if ($data) {
                      $basename[$item_index] = $data->getName();
                    }

                    $item_index++;

                    if (count($valuesReference) == $item_index) {
                      $row[] = implode("|", $basename);
                    }
                    break;
                  case 'node':
                    $data = $this->entityTypeManager->getStorage('node')
                      ->load($item->id());
                    if ($data) {
                      $row[] = $data->getTitle();
                    }
                    $item_index++;
                    break;
                }
              }
            } else {
              $row[] = "";
            }
            break;

          case 'list_string':
          case 'text_long':
            $row[] = $node->get($field)->value;
            break;

          case 'datetime':
            $values = $node->get($field)->getValue();
            if(count($values) > 1) {
              $val = [];
              foreach ($values as $value) {
                $val[] = $value['value'];
              }

              $row[] = implode("|", $val);
            } else {
              $row[] = $node->get($field)->value;
            }
            break;

          default:
            $row[] = $node->get($field)->value;
            break;
        }
      }

      if (!empty($export_body)) {
        $row[] = ($node->get('body')->summary) ? $node->get('body')->summary : '';
        $row[] = ($node->get('body')->value) ? $node->get('body')->value : '';
      }
      /**kint($row);
      die(0);*/
      fputcsv($file, $row);
    }


    fclose($file);

    return $pathRelative;
  }

  public function createTarballExportMedia($filename, $content_type){
    $pathAbsolute = $this->folders['public_migrate']['absolute']  . "/$filename";
    $pathRelative = $this->folders['public_migrate']['relative']  . "/$filename";
    $mediaPath = $this->folders['export_media']['absolute'] . "/$content_type";

    file_unmanaged_delete($pathAbsolute);

    //$archiver = new ArchiveTar($pathAbsolute, 'gz');
    //$archiver->addModify(["$mediaPath"], "$content_type", "$mediaPath");

    $archiver = new \ZipArchive();
    $archiver->open($pathAbsolute, \ZipArchive::CREATE);
    $handle = opendir($mediaPath);
    while ($f = readdir($handle)) {
      if ($f != "." && $f != "..") {
        if (is_file($mediaPath ."/". $f)) {
          $archiver->addFile($mediaPath . "/" . $f, $content_type . "/" . basename($f));
        }
      }
    }
    $archiver->close();

    file_unmanaged_delete_recursive($mediaPath);

    return $pathRelative;
  }

  public function createFolderExportImport() {
    $srcPublic = "public://migrate";
    $srcMigrate = $this->fileSystem->realpath($srcPublic);

    $folders = [
      "public_migrate" => ['relative' => $srcPublic, 'absolute' => $srcMigrate],
      "export_data" => ['relative' => $srcPublic . "/export-data", 'absolute' => $srcMigrate . '/export-data' ],
      "export_media" => ['relative' => $srcPublic . "/export-media", 'absolute' => $srcMigrate . '/export-media' ],
      "import_data" => ['relative' => $srcPublic . "/import-data", 'absolute' => $srcMigrate . '/import-data' ],
      "import_media" => ['relative' => $srcPublic . "/import-media", 'absolute' => $srcMigrate . '/import-media' ]
    ];

    if (!is_dir($folders['public_migrate']['absolute'])) {
      mkdir($folders['public_migrate']['absolute'], 0777, TRUE);
      chmod($folders['public_migrate']['absolute'], 0777);
    }

    if (!is_dir($folders['export_data']['absolute'])) {
      mkdir($folders['export_data']['absolute'], 0777, TRUE);
      chmod($folders['export_data']['absolute'], 0777);
    }

    if (!is_dir($folders['export_media']['absolute'])) {
      mkdir($folders['export_media']['absolute'], 0777, TRUE);
      chmod($folders['export_media']['absolute'], 0777);
    }

    if (!is_dir($folders['import_data']['absolute'])) {
      mkdir($folders['import_data']['absolute'], 0777, TRUE);
      chmod($folders['import_data']['absolute'], 0777);
    }

    if (!is_dir($folders['import_media']['absolute'])) {
      mkdir($folders['import_media']['absolute'], 0777, TRUE);
      chmod($folders['import_media']['absolute'], 0777);
    }

    return $folders;
  }

  public function createCsvFileExportMenuData($filename, $fields) {
    $pathAbsolute = $this->folders['public_migrate']['absolute'] . "/$filename";
    $pathRelative = $this->folders['public_migrate']['relative'] . "/$filename";

    file_unmanaged_delete($pathAbsolute);

    $file = fopen($pathAbsolute, 'w');

    $column = [];
    $column[] = 'id';
    $column[] = 'uuid';
    $column[] = 'langcode';
    $column[] = 'bundle';
    $column[] = 'enabled';
    $column[] = 'title';
    $column[] = 'description';
    $column[] = 'menu_name';
    $column[] = 'link';
    $column[] = 'external';
    $column[] = 'rediscover';
    $column[] = 'weight';
    $column[] = 'expanded';
    $column[] = 'parent';
    $column[] = 'changed';
    $column[] = 'default_langcode';

    fputcsv($file, $column);

    foreach($fields as $menu){
      $menuLinkIds = \Drupal::entityQuery('menu_link_content')
        ->condition('menu_name',$menu)
        ->execute();

      $menuLinks = MenuLinkContent::loadMultiple($menuLinkIds);

      foreach($menuLinks as $link){
        $row = [];
        $row[] = $link->get('id')->value;
        $row[] = $link->get('uuid')->value;
        $row[] = $link->get('langcode')->value;
        $row[] = $link->get('bundle')->value;
        $row[] = $link->get('enabled')->value;
        $row[] = $link->get('title')->value;
        $row[] = $link->get('description')->value;
        $row[] = $link->get('menu_name')->value;
        $row[] = $link->get('link')->uri;
        $row[] = $link->get('external')->value;
        $row[] = $link->get('rediscover')->value;
        $row[] = $link->get('weight')->value;
        $row[] = $link->get('expanded')->value;
        $row[] = $link->get('parent')->value;
        $row[] = $link->get('changed')->value;
        $row[] = $link->get('default_langcode')->value;

        fputcsv($file, $row);
      }
    }

    fclose($file);

    return $pathRelative;
  }

  /**
   * Lote finalizado.
   */
  public static function importFromCsvFinishedCallback($success, $results, $operations) {
    $messenger = \Drupal::messenger();
    if ($success) {
      $messenger->addMessage(t('@count procesados.', ['@count' => $results]));
    }
    else {
      $messenger->addMessage(t('Finalizado con error.'));
    }
  }

  /**
   * Esta función ejecuta procesos en lote y crea nuevos nodos.
   */
  public static function importFromCsv($row, &$context) {
    $operationDetails = '';
    $keyIndex = $row['key_index'];
    $data = $row['data'];
    $contentType = $row['content_type'];
    $fields = $row['fields'];

    $fieldNames = $fields['name'];
    $fieldTypes = $fields['type'];
    $fieldSettings = $fields['setting'];

    $nodeArray = [];

    $nodeArray['type'] = strtolower($contentType);
    $nodeArray['promote'] = 0;

    $node = Node::create($nodeArray);
    $node->save();

    unset($node);

    $operationDetails = ' Importado exitosamente.';

    $context['message'] = t('Ejecutando lote "@id" @details', ['@id' => $data[$keyIndex]['title'], ' @details' => $operationDetails]);
    $context['results'] = $row['result'];
  }

  /**
   * Esta funcion ejecuta procesos en lote y crea nuevo nodos
   */
  public static function importImageFromCsv($row, &$context) {

    $operationDetails = '';
    $cl = $row['columns'];
    $data = $row['data'];

    $bodySummary = (!empty($cl['body_summary'])) ? html_entity_decode($data[$cl['body_summary']]) : '';
    $bodyValue = (!empty($cl['body_description'])) ? html_entity_decode($data[$cl['body_description']]) : '';

    if (!empty($data[$cl['nid']]) && ($nid = self::checkExitingNode($data[$cl['nid']], $row['content_type'], 'nid'))  ) {
      $node = Node::load($nid);
      if (!empty($node)) {
        $node->set('title', Html::escape($data[$cl['title']]));
        if (!empty($row['save_body']) && !empty($bodyValue)) {
          $node->set('body', [
              'value' => $bodyValue,
              'summary' => $bodySummary,
              'format' => 'full_html',
            ]
          );
        }
        if (!empty($data[$cl['img_src']])) {
          $mediaImage = @self::uploadOffersMedia($data[$cl['img_src']], $data[$cl['img_alt']], Html::escape($data[$cl['title']]), $row['import_media']);
          if (!empty($mediaImage)) {
            $image_field_name = $row['fields']['field_prensa_images'];
            $node->{$image_field_name}->setValue($mediaImage);
          }
        }
        $operationDetails = ' Actualizado exitosamente.';
        $node->save();
        unset($node);
      }
    } elseif ((!empty($data[$cl['nid']]) && !empty($data[$cl['title']])) && !empty($row['new_node'])) {
      $nodeObject = @self::checkExitingNode($data[$cl['title']], $row['content_type'], 'title');
      if (empty($nodeObject)) {
        $mediaImage = @self::uploadOffersMedia($data[$cl['img_src']], $data[$cl['img_alt']], Html::escape($data[$cl['title']]), $row['import_media']);

        $taxoTags = @self::handleTaxonomyTerm($data[$cl['field_tags']], 'tags', true);
        $taxoRegions = @self::handleTaxonomyTerm($data[$cl['field_region']], 'regions', FALSE);

        $image_field_name = $row['fields']['field_prensa_images'];
        $tags_field_name = $row['fields']['field_prensa_tags'];
        $regions_field_name = $row['fields']['field_prensa_region'];
        $created_field_name = $row['fields']['field_prensa_date'];
        $state_field_name = $row['fields']['field_prensa_state'];

        $nodeArray = [
          'type' => $row['content_type'],
          'title' => Html::escape($data[$cl['title']]),
        ];

        if (!empty($row['save_body']) && !empty($bodyValue)) {
          $nodeArray = array_merge($nodeArray, [
              'body' => [
                'value' => $bodyValue,
                'summary' => $bodySummary,
                'format' => 'full_html',
              ]
          ]);
        }

        if (!empty($row['save_created']) && $state_field_name) {
          $nodeArray = array_merge($nodeArray, [$created_field_name => [ 'value' => date("Y-m-d\TH:i:s", $data[$cl['created']]) ]]);
        }

        if (!empty($mediaImage)) {
          $nodeArray = array_merge($nodeArray, [$image_field_name => $mediaImage]);
        }

        if (!empty($taxoRegions)) {
          $nodeArray = array_merge($nodeArray, [$regions_field_name => $taxoRegions]);
        }

        if (!empty($taxoTags)) {
          $nodeArray = array_merge($nodeArray, [$tags_field_name => $taxoTags]);
        }

        if (!empty($state_field_name)) {
          $nodeArray = array_merge($nodeArray, [$state_field_name => 'AC']);
        }

        $node = Node::create($nodeArray);

        $operationDetails = ' Importado exitosamente.';

        $node->save();

        unset($node);
      }
    }

    $context['message'] = t('Ejecutando lote "@id" @details', ['@id' => $data[$cl['title']], '@details' => $operationDetails]);
    $context['results'] = $row['result'];
  }

  /**
   * Importar menu de archivo csv.
   */
  public static function importMenuFromCsv($row, &$context) {
    $operationDetails = '';
    $cl = $row['columns'];
    $data = $row['data'];

    $menuName = $data[$cl['menu_name']];
    $menu = \Drupal::entityTypeManager()
      ->getStorage('menu')
      ->load($menuName);

    if (empty($menu)) {
      $menu = \Drupal::entityTypeManager()
        ->getStorage('menu')
        ->create([
          'id' => $menuName,
          'label' => $menuName,
          'expanded' => TRUE
        ])->save();
    }

    $menuLinkEntity = \Drupal::entityQuery('menu_link_content')
      ->condition('uuid', $data[$cl['uuid']])
      ->execute();

    if (!$menuLinkEntity) {
      $menuLinkEntity = MenuLinkContent::create([
        'title' => $data[$cl['title']],
        'uuid' => $data[$cl['uuid']],
        'link' => ['uri' => 'internal:#'],
        'menu_name' => $data[$cl['menu_name']],
        'weight' => $data[$cl['weight']],
        'parent' => $data[$cl['parent']],
        'expanded' => $data[$cl['expanded']],
      ]);

      $operationDetails = ' Importado exitosamente.';
    } else {
      $menuLinkEntity = MenuLinkContent::load(reset($menuLinkEntity));
      $operationDetails = ' Actualizado exitosamente.';
    }

    $menuLinkEntity->save();
    unset($menuLinkEntity);

    $context['message'] = t('Ejecutando lote "@id" @details', ['@id' => $data[$cl['title']], '@details' => $operationDetails]);
    $context['results'] = $row['result'];
  }

  /**
   * Verifica si existe nodo en CMS.
   *
   */
  public static function checkExitingNode($item_code, $node_type, $field_name) {
    $nodes = \Drupal::entityQuery('node')->condition('type', $node_type)->condition($field_name, $item_code, 'IN')->execute();
    $nid = key($nodes);
    if (!empty($nodes)) {
      return $nodes[$nid];
    }
  }

  /**
   *  Obtener referencia de taxonomia.
   */
  public function getTermReference($voc, $terms) {
    $vocName = strtolower($voc);
    $vid = preg_replace('@[^a-z0-9_]+@', '_', $vocName);
    $vocabularies = Vocabulary::loadMultiple();


    if (!isset($vocabularies[$vid])) {
      // Crear vocabulario
      @self::createVoc($terms, $vid);
    }

    $termArray = array_map('trim', explode('|', $terms));

    $termIds = [];

    foreach ($termArray as $term) {
      $termId = @self::getTermId($term, $vid);
      if (empty($termId)) {
        $termId = @self::createTerm($voc, $term, $vid);
      }
      $termIds[]['target_id'] = $termId;
    }
    return $termIds;
  }

  /**
   * Crear terminos si no esta disponible.
   */
  public function createVoc($vid, $voc) {
    $vocabulary = Vocabulary::create([
      'vid' => $vid,
      'machine_name' => $vid,
      'name' => $voc
    ]);

    $vocabulary->save();
  }

  /**
   * Crear terminos si no esta disponible.
   */
  public function createTerm($voc, $term, $vid) {
    Term::create([
      'parent' => [$voc],
      'name' => $term,
      'vid' => $vid
    ])->save();

    $termId = @self::getTermId($term, $vid);
    return $termId;
  }

  /**
   * Obtiene TermId valido.
   */
  public function getTermId($term, $vid) {
    $query = \Drupal::entityQuery('taxonomy_term');
    $query->condition('vid', $vid);
    $query->condition('name', $term);

    $termResult = $query->execute();
    return key($termResult);
  }

  /**
   * Terminos de taxonomia.
   */
  public function handleTaxonomyTerm($row_name, $vid, $muliple = false) {
    $multiTerms = explode('|', $row_name);
    $terms = [];

    if (count($multiTerms) >= 1) {

      $vocabulary = Vocabulary::load($vid);

      if (empty($vocabulary)) {
        $vocabulary = Vocabulary::create([
          'vid' => $vid,
          'name' => ucwords($vid)
        ]);
        $vocabulary->save();
      }

      $i = 0;
      foreach ($multiTerms as $multi_term) {
        if (empty($multi_term)){
          continue;
        }
        $query = \Drupal::entityQuery('taxonomy_term');
        $query->condition('vid', $vocabulary->id());
        $query->condition('name', $multi_term);
        $term = $query->execute();
        if (!empty($term)) {
          if ($muliple) {
            $terms[$i] = [
              'target_id' => key($term)
            ];
          } else {
            return [
              'target_id' => key($term)
            ];
          }
        } else {
          $term = Term::create([
            'vid' => $vocabulary->id(),
            'name' => $multi_term,
          ]);
          $term->save();
          if ($muliple) {
            $terms[$i] = [
              'target_id' => $term->id()
            ];
          } else {
            return [
              'target_id' => $term->id()
            ];
          }
        }
        $i++;
      }
    }
    return $terms;
  }

  /**
   * Cargado de archivo de imagenes.
   */
  public function uploadOffersMedia($file_name, $alt, $title, $folder_media) {
    $multiImage = explode('|', $file_name);
    $multiAlt = explode('|', $alt);
    $multiTitle = explode('|', $title);
    if (count($multiImage) >= 1) {
      $i = 0;
      $mediaImage = null;
      foreach ($multiImage as $value) {
        $imageLocalDir = $folder_media;
        // Guardar imagen en local de datos remoto
        $uri = $filename = null;
        foreach (file_scan_directory($imageLocalDir,  "/^$value$/") as $uri_ => $file) {
          $extension = strstr(pathinfo($uri_)['basename'], '.');
          $filename = 'IM-'. date('YmdHis') .'-' . str_replace('.', '', microtime(true)) . $extension;
          $uri = dirname($uri_). '/' . $filename;
          rename($uri_, $uri);
          break;
        }

        if (!empty($uri)) {
          $dirPrensa = "public://senapi/prensa/" . date("Y-m");
          $data = file_get_contents($uri);
          $file = file_save_data($data,  $dirPrensa . '/' . $filename, FILE_EXISTS_RENAME);
          $mediaImage[$i] = [
            'target_id' => $file->id(),
            'alt' => $title,
            'title' => $title
          ];
        }
        $i++;
      }
      return $mediaImage;
    }
  }

  /**
   * Cargado de imagenes.
   */
  public function uploadImageMedia($image_src, $image_alt, $image_title, $dir_extract, $setting) {

    $imageSrc = explode('|', $image_src);
    $imageAlt = explode('|', $image_alt);
    $imageTitle = explode('|', $image_title);

    $destination = @self::getUploadLocation($setting);

    if (!file_prepare_directory($destination, FILE_CREATE_DIRECTORY)) {
      throw new HttpException(500, 'La ruta del archivo de destino no se puede escribir.');
    }

    @chmod($destination, 0777);

    if (count($imageSrc)) {
      $media = [];
      foreach ($imageSrc as $srcKey => $srcValue) {
        $uriFile = null;
        foreach (file_scan_directory($dir_extract, "/^$srcValue$/") as $uri => $file) {
          $uriFile = $uri;
        }

        if (!empty($uriFile)) {
          $file = file_save_data(file_get_contents($uriFile), $destination . '/'. $srcValue, FILE_EXISTS_RENAME);
          $imageArray  = [
            'target_id' => $file->id(),
            'alt' => $imageAlt[$srcKey],
            'title' => $imageTitle[$srcKey]
          ];

          if (!isset($imageAlt[$srcKey])) {
            $imageArray = array_merge($imageArray, ['alt' => $imageAlt[0]]);
          }

          if (!isset($imageTitle[$srcKey])) {
            $imageArray = array_merge($imageArray, ['alt' => $imageTitle[0]]);
          }

          $media[] = $imageArray;
        }
      }
      return $media;
    }
    return [];
  }

  /**
   * Cargado de archivos.
   */
  public function uploadFileMedia($file_src, $file_description, $dir_extract, $setting) {
    $fileSrc = explode('|', $file_src);
    $fileDescription = explode('|', $file_description);

    $destination = @self::getUploadLocation($setting);

    if (!file_prepare_directory($destination, FILE_CREATE_DIRECTORY)) {
      throw new HttpException(500, 'La ruta del archivo de destino no se puede escribir.');
    }

    @chmod($destination, 0777);

    if (count($fileSrc)) {
      $media = [];
      foreach ($fileSrc as $srcKey => $srcValue) {
        $uriFile = null;
        foreach (file_scan_directory($dir_extract, "/^$srcValue$/") as $uri => $file) {
          $uriFile = $uri;
        }

        if (!empty($uriFile)) {
          $file = file_save_data(file_get_contents($uriFile), $destination . '/'. $srcValue, FILE_EXISTS_RENAME);
          $fileArray = [
            'target_id' => $file->id(),
            'description' => $fileDescription[$srcKey]
          ];

          if (!isset($fileDescription[$srcKey])) {
            $fileArray = array_merge($fileArray, ['alt' => $fileDescription[0]]);
          }

          $media[] = $fileArray;
        }
      }
      return $media;
    }
    return [];
  }

  public function uploadFileMediaCustomName($file_src, $file_description, $dir_extract, $setting, array $attr) {
    $fileSrc = explode('|', $file_src);
    $fileDescription = explode('|', $file_description);

    $destination = @self::getUploadLocation($setting);

    if (!file_prepare_directory($destination, FILE_CREATE_DIRECTORY)) {
      throw new HttpException(500, 'La ruta del archivo de destino no se puede escribir.');
    }

    @chmod($destination, 0777);

    if (count($fileSrc)) {
      $media = [];
      foreach ($fileSrc as $srcKey => $srcValue) {
        $uriFile = null;
        foreach (file_scan_directory($dir_extract, "/^$srcValue$/") as $uri => $file) {
          $uriFile = $uri;
        }

        if (!empty($uriFile)) {
          $file = file_save_data(file_get_contents($uriFile), $destination . '/'. $attr['name']. $attr['number'] . '_' . $attr['date'] .'.' . $attr['extension'], FILE_EXISTS_RENAME);
          $fileArray = [
            'target_id' => $file->id(),
            'description' => $fileDescription[$srcKey]
          ];

          if (!isset($fileDescription[$srcKey])) {
            $fileArray = array_merge($fileArray, ['alt' => $fileDescription[0]]);
          }

          $media[] = $fileArray;
        }
      }
      return $media;
    }
    return [];
  }
  /**
   * @param array $settings
   *
   * @return string
   */
  public function getUploadLocation(array $settings) {
    $destination = trim($settings['file_directory'], '/');

    $destination = PlainTextOutput::renderFromHtml($this->token->replace($destination, []));
    return $settings['uri_scheme'] . '://' . $destination;
  }
  /**
   * Return csv data.
   *
   * @param resource $handle
   *   Resource handler.
   *
   * @return array
   *   Array of csv row.
   */
  public function getCsvData($handle) {
    return fgetcsv($handle, 0, ',', '"');
  }

  /**
   * Get file handle from uri.
   *
   * @param string $uri
   *   URI of file.
   *
   * @return resource
   *   Resource handler.
   */
  public function getFileHandler($uri) {
    return fopen($this->fileSystem->realpath($uri), "r");
  }

  /**
   * Get an appropriate archiver class for the file.
   *
   * @param string $file
   *   The file path.
   */
  public function getArchiver($file) {
    $extension = strstr(pathinfo($file)['basename'], '.');
    switch ($extension) {
      case '.tar.gz':
      case '.tar':
        $this->archiver = new \PharData($file);
        break;

      case '.zip':
        $this->archiver = new \ZipArchive($file);
        $this->archiver->open($file);
      default:
        break;
    }
    return $this->archiver;
  }
}