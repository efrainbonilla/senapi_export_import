<?php

namespace Drupal\senapi_export_import\Form;


use Drupal\Component\Utility\Html;
use Drupal\Core\File\FileSystem;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Session\AccountProxy;
use Drupal\Core\Url;
use Drupal\file\Entity\File;
use Drupal\node\Entity\Node;
use Drupal\senapi_export_import\EntitySaveHelper;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use \Drupal\system\Entity\Menu;
/**
 * Class EntitySaveForm.
 *
 * @package Drupal\senapi_export_import\Form
 */
class ExportImportForm extends FormBase {

  /**
   * Drupal File System.
   *
   * @var \Drupal\Core\File\FileSystem
   */
  protected $fileSystem;

  /**
   * A instance of the senapi_export_import helper services.
   *
   * @var \Drupal\senapi_export_import\EntitySaveHelper
   */
  protected $entityHelper;

  /**
   * A instance of the EntityTypeManagerInterface.
   *
   * @var \Drupal\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Drupal\Core\Session\AccountProxy definition.
   *
   * @var \Drupal\Core\Session\AccountProxy
   */
  protected $currentUser;

  /**
   * {@inheritdoc}
   */
  public function __construct(
    FileSystem $file_system,
    EntitySaveHelper $entityHelper,
    EntityTypeManagerInterface $entityTypeManager,
    AccountProxy $current_user) {

    $this->fileSystem = $file_system;
    $this->entityHelper = $entityHelper;
    $this->entityTypeManager = $entityTypeManager;
    $this->currentUser = $current_user;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('file_system'),
      $container->get('senapi_export_import.batch_export_import'),
      $container->get('entity_type.manager'),
      $container->get('current_user')
    );
  }

  /**
   * Returns a unique string identifying the form.
   *
   * The returned ID should be a unique string that can be a valid PHP function
   * name, since it's used in hook implementation names such as
   * hook_form_FORM_ID_alter().
   *
   * @return string
   *   The unique string identifying the form.
   */
  public function getFormId() {
    return 'export_import_form';
  }

  /**
   * Form constructor.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   *
   * @return array
   *   The form structure.
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    //Seleccione tipo de acción
    $form['action_type'] = [
      '#type' => 'select',
      '#title' => 'Tipo de acción',
      '#description' => 'Seleccione una opción',
      '#options' => [
        'export' => 'Exportar',
        'import' => 'Importar',
        'delete' => 'Eliminar sin usar',
        'export_menu' => 'Exportar Menu',
        'import_menu' => 'Importar Menu'
      ],
      '#weight' => 1,
    ];

    //Cargar archivo csv
    $form['upload_csv'] = [
      '#type' => 'managed_file',
      '#title' => 'Seleccione archivo CSV',
      '#description' => 'Por favor cargue el archivo CSV generado por la acción exportar.',
      '#upload_location' => $this->entityHelper->folders['import_data']['relative'],
      '#upload_validators' => [
        'file_validate_extensions' => ['csv'],
      ],
      '#weight' => 2,
    ];

    //Cargar archivo zip
    $form['upload_zip'] = [
      '#type' => 'managed_file',
      '#title' => 'Seleccione archivo ZIP',
      '#description' => 'Por favor cargue el archivo ZIP generado por la acción exportar.',
      '#upload_location' => $this->entityHelper->folders['import_media']['relative'],
      '#upload_validators' => [
        'file_validate_extensions' => ['zip'],
      ],
      '#weight' => 2.5,
    ];

    //Obtener tipo de contenido
    $contentTypes = $this->entityHelper->getAllContentTypes();
    $selected = !empty($form_state->getValue('content_types')) ? $form_state->getValue('content_types') : key($contentTypes);
    $form['content_types'] = [
      '#type' => 'select',
      '#title' => 'Tipo de Contenido',
      '#description' => 'Por favor seleccione tipo de contenido para importar.',
      '#default_value' => $selected,
      '#options' => $contentTypes,
      '#states' => [
        'invisible' => [':input[name="action_type"]' => [['value' => 'delete'], ['value' => 'export_menu'], ['value' => 'import_menu']]],

      ],
      '#ajax' => [
        'callback' => '::ajaxDependentDropdownCallback',
        'wrapper' => 'dropdown-second-replace',
      ],
      '#requered' => TRUE,
      '#weight' => 3,
    ];

    $options = $this->entityHelper->getAllItemFields($selected, 'node');

    #Obtener campo tipo de contendo
    $form['content_types_fields'] = [
      '#type' => 'select',
      '#title' => $contentTypes[$selected] . ' ',
      '#description' => 'Por favor seleccione campos de tipo de contenido.',
      '#default_value' => $selected,
      '#options' => $options,
      '#multiple' => TRUE,
      '#size' => 10,
      '#states' => [
        'invisible' => [':input[name="action_type"]' => [['value' => 'delete'], ['value' => 'export_menu'], ['value' => 'import_menu']]],
      ],
      '#prefix' => '<div id="dropdown-second-replace">',
      '#suffix' => '</div>',
      '#weight' => 4,
    ];

    //Descargar archivo simple
    $form['delete_confirm'] = [
      '#type' => 'checkbox',
      '#title' => 'Esta seguro de eliminar ?',
      '#description' => 'Esta acción no se puede deshacer.',
      '#states' => [
        'visible' => [':input[name="action_type"]' => [['value' => 'delete']]],
      ],
      '#weight' => 5,
    ];

    //Exportar cuerpo y resumen
    $form['export_body'] = [
      '#type' => 'checkbox',
      '#title' => 'Cuerpo y resumen',
      '#description' => 'Incluir cuerpo y resumen.',
      '#states' => [
        'invisible' => [':input[name="action_type"]' => [['value' => 'delete'], ['value' => 'export_menu'], ['value' => 'import_menu']]],
      ],
      '#weight' => 6,
    ];

    # Fecha creado
    $form['export_created'] = [
      '#type' => 'checkbox',
      '#title' => 'Fecha creado',
      '#description' => 'Incluir fecha creado.',
      '#states' => [
        'visible' => [':input[name="action_type"]' => ['value' => 'import']],
      ],
      '#weight' => 6.5,
    ];

    #Crear nodo basado en titulo
    $form['new_node'] = [
      '#type' => 'checkbox',
      '#title' => 'Crear nuevo nodo',
      '#description' => 'Si nid no está presente en csv, cree un nuevo nodo con los campos title, summary, body e images.',
      '#states' => [
        'visible' => [':input[name="action_type"]' => ['value' => 'import']],
      ],
      '#weight' => 7,
    ];

    //Obtener menu ids
    $menuObject = Menu::loadMultiple();

    $menuTypes = array_map(function ($menuEnt) {
      return $menuEnt->label();
    }, $menuObject);
    $selected = !empty($form_state->getValue('menu_types')) ? $form_state->getValue('menu_types') : key($menuTypes);

    $form['menu_types'] = [
      '#type' => 'select',
      '#title' => 'Exportar menu',
      '#description' => 'Por favor seleccione tipo de menu.',
      '#default_value' => $selected,
      '#options' => $menuTypes,
      '#multiple' => TRUE,
      '#size' => 10,
      '#states' => [
        'visible' => [':input[name="action_type"]' => [['value' => 'export_menu']]],
      ],
      '#requered' => TRUE,
      '#weight' => 8,
    ];

    # Btn importar csv
    $form['import']['#type'] = 'actions';

    $form['import']['submit'] = [
      '#type' => 'submit',
      '#value' => 'Enviar',
      '#button_type' => 'primary',
    ];

    //Agregar libreria css/js en formulario
    $form['#attached']['library'][] = 'senapi_export_import/import_images';

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function ajaxDependentDropdownCallback(array &$form, FormStateInterface $form_state) {
    return $form['content_types_fields'];
  }

  /**
   * @param array $form
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    // Validar confirmación eliminar
    $deleteConfirm = $form_state->getValue('delete_confirm');
    $actionType = $form_state->getValue('action_type');
    if (empty($deleteConfirm) && $actionType == 'delete') {
      $form_state->setErrorByName('delete_confirm', 'Por favor revisa la confirmación.');
    }

    // Validar importar campo csv
    $uploadCsv = $form_state->getValue('upload_csv');
    if (empty($uploadCsv) && $actionType == 'import') {
      $form_state->setErrorByName('upload_csv', 'Por favor cargue un archivo CSV.');
    }

    // Validar importar campo zip
    $uploadZip = $form_state->getValue('upload_zip');
    if (empty($uploadZip) && $actionType == 'import') {
      $form_state->setErrorByName('upload_zip', 'Por favor cargue un archivo TAR.GZ.');
    }

    // Validar importar campo menu_types
    $menuTypes = $form_state->getValue( 'menu_types');
    if ((empty($menuTypes) && $actionType == 'export_menu') ) {
      $form_state->setErrorByName('menu_types', 'Por favor seleccione una opción de menu a exportar.');
    }
  }

  /**
   * Controlador de envío de formularios.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $current = $this->currentUser->id();

    $actionType = $form_state->getValue('action_type');
    $contentTypes = $form_state->getValue('content_types');
    $fields = $form_state->getValue('content_types_fields');
    $exportBody = $form_state->getValue('export_body');

    switch ($actionType) {
      case 'export':
        $filenameData =  $contentTypes . '_' . $current . '.csv';
        $pathData = $this->entityHelper->createCsvFileExportData($filenameData, $contentTypes, $fields, $exportBody);

        $filenameMedia =  $contentTypes . '_' . $current . '.zip';
        $pathMedia = $this->entityHelper->createTarballExportMedia($filenameMedia, $contentTypes);

        $urlData = file_create_url($pathData);
        $urlMedia = file_create_url($pathMedia);

        $response = 'Descargar <a href="' . $urlData . '" target="_blank">' . $filenameData . '</a> <br>';
        $response .= 'Descargar <a href="' . $urlMedia . '" target="_blank">' . $filenameMedia . '</a>';
        drupal_set_message(t($response));
        break;

      case 'import':
        $uploadZip = $form_state->getValue('upload_zip');
        $uploadZipFile = $this->entityTypeManager->getStorage('file')->load($uploadZip[0]);
        $archiver = $this->entityHelper->getArchiver($this->fileSystem->realpath($uploadZipFile->getFileUri()));

        $archiver->extractTo($this->entityHelper->folders['import_media']['relative']);

        $dirExtract =  $this->entityHelper->folders['import_media']['relative'] . '/' . $contentTypes;
        if (!is_dir($dirExtract)){
          $dirExtract =  $this->entityHelper->folders['import_media']['relative'];
        }

        $uploadCsv = $form_state->getValue('upload_csv');
        $uploadCsvFile = $this->entityTypeManager->getStorage('file')->load($uploadCsv[0]);
        $handle = $this->entityHelper->getFileHandler($uploadCsvFile->getFileUri());

        $fields = $this->entityHelper->getFields($contentTypes);

        $firstRow = $this->entityHelper->getCsvData($handle);

        $fieldNames = $fields['name'];
        $fieldProperties = $fields['properties'];

        $keyIndex = [];
        foreach ($firstRow as $dataKey => $dataValue) {
          $dataValueOrigin = $dataValue;
          $row = explode('_', $dataValue);
          unset($row[0]);

          $dataValue = join("_", $row);

          if (in_array($dataValue, $fieldNames)) {

            $keyIndex[$dataValue]['key'] = $dataKey;
            $keyIndex[$dataValue]['properties'] = $fieldProperties[array_search($dataValue, $fieldNames)];

            if (array_search($dataValueOrigin . '_alt', $firstRow)){
              $keyIndex[$dataValue]['alt'] = [
                'key' => array_search($dataValueOrigin . '_alt', $firstRow)
              ];
            }

            if (array_search($dataValueOrigin . '_title', $firstRow)) {
              $keyIndex[$dataValue]['title'] = [
                'key' => array_search($dataValueOrigin . '_title', $firstRow)
              ];
            }

            if (array_search($dataValueOrigin . '_description', $firstRow)) {
              $keyIndex[$dataValue]['description'] = [
                'key' => array_search($dataValueOrigin . '_description', $firstRow)
              ];
            }

          } else {
            if (in_array('body', $fieldNames)){
              $bodyKey = array_search('body', $fieldNames);
              $keyIndex['body']['key'] = $dataKey;
              $keyIndex['body']['properties'] = $fieldProperties[$bodyKey];

              if ($dataValue == 'body_description') {
                $keyIndex['body']['value'] = ['key' => $dataKey];
              } elseif ($dataValue == 'body_summary') {
                $keyIndex['body']['summary'] = ['key' => $dataKey];
              }
            }
          }
        }

        //kint($fieldProperties);
        //kint($keyIndex);
        //die(0);
        $nodes = [];
        $nodeArray = [];

        while ($data = $this->entityHelper->getCsvData($handle)) {

          foreach ($keyIndex as $itemKey => $itemValue) {
            if ($data[$itemValue['key']]) {
              $type = $itemValue['properties']['type'];
              switch ($type) {
                case 'image':
                  $imageSrc = $data[$itemValue['key']];
                  $imageAlt = $nodeArray['title'];
                  $imageTitle = $nodeArray['title'];

                  if (isset($itemValue['alt']) && isset($itemValue['alt']['key']) && !empty($data[$itemValue['alt']['key']])) {
                    $imageAlt = Html::escape($data[$itemValue['alt']['key']]);
                  }

                  if (isset($itemValue['title']) && isset($itemValue['title']['key']) && !empty($data[$itemValue['title']['key']])) {
                    $imageTitle = Html::escape($data[$itemValue['title']['key']]);
                  }

                  $images = $this->entityHelper->uploadImageMedia($imageSrc, $imageAlt, $imageTitle, $dirExtract, $itemValue['properties']['setting']);

                  if ($images) {
                    $nodeArray[$itemKey] = $images;
                  }
                  break;

                case 'file':
                  $fileDescription = $nodeArray['title'];
                  if (isset($itemValue['description']) && isset($itemValue['description']['key']) && !empty($data[$itemValue['description']['key']])) {
                    $fileDescription = Html::escape($data[$itemValue['description']['key']]);
                  }

                  $fileSrc = $data[$itemValue['key']];

                  switch ($itemKey) {
                    case 'field_gaceta_patente_file':
                    case 'field_gaceta_signo_file':
                    case 'field_gaceta_dautor_file':
                      if ($itemKey == 'field_gaceta_patente_file') {
                        switch ($fileDescription) {
                          case 'TOMO I':
                          case 'TOMO II':
                          case 'TOMO III':
                            $name = 'Tomo';
                            break;
                          default:
                            $name = 'Patentes';
                            break;
                        }
                      }

                      if ($itemKey == 'field_gaceta_signo_file') {
                        switch ($fileDescription) {
                          case 'TOMO I':
                          case 'TOMO II':
                          case 'TOMO III':
                            $name = 'Tomo';
                            break;
                          default:
                            $name = 'Signos';
                            break;
                        }
                      }
                      if ($itemKey == 'field_gaceta_dautor_file') {
                        switch ($fileDescription) {
                          case 'TOMO I':
                          case 'TOMO II':
                          case 'TOMO III':
                            $name = 'Tomo';
                            break;
                          default:
                            $name = 'Dautor';
                            break;
                        }
                      }
                      $date = new \DateTime($data[$keyIndex['field_gaceta_date']['key']]);
                      $attr = ['name' => $name, 'number' => $data[$keyIndex['field_gaceta_number']['key']], 'date' => $date->format('dmY'), 'extension' => 'pdf'];
                      $files = $this->entityHelper->uploadFileMediaCustomName($fileSrc, $fileDescription, $dirExtract, $itemValue['properties']['setting'], $attr);
                      break;

                    case 'field_oposicion_signo_file':
                    case 'field_oposicion_patente_file':
                      if ($itemKey == 'field_oposicion_patente_file') {
                       $name = 'OP_Patentes';
                      }

                      if ($itemKey == 'field_oposicion_signo_file') {
                        $name = 'OP_Signos';
                      }

                      $title = $data[$keyIndex['field_oposicion_gaceta']['key']];

                      $node = \Drupal::entityQuery('node')
                        ->condition('type', 'gaceta')
                        ->condition('title', $title, 'IN')
                        ->execute();

                      if ($node) {
                        $node = Node::load(key($node));
                        $gaceta = $node->get('field_gaceta_number')->first()->getValue()['value'];
                        $gacetaDate = $node->get('field_gaceta_date')->first()->getValue()['value'];

                        $date = new \DateTime($gacetaDate);
                        $attr = ['name' => $name, 'number' => $gaceta, 'date' => $date->format('dmY'), 'extension' => 'pdf'];
                        $files = $this->entityHelper->uploadFileMediaCustomName($fileSrc, $fileDescription, $dirExtract, $itemValue['properties']['setting'], $attr);
                      }
                      break;

                    default:
                      $files = $this->entityHelper->uploadFileMedia($fileSrc, $fileDescription, $dirExtract, $itemValue['properties']['setting']);
                      break;
                  }

                  if ($files) {
                    $nodeArray[$itemKey] = $files;
                  }
                  break;

                case 'entity_reference':

                  if ($itemValue['properties']['setting']['target_type'] == 'taxonomy_term') {
                    $term = $data[$itemValue['key']];
                    $voc = key($itemValue['properties']['setting']['handler_settings']['target_bundles']);
                    if ($voc) {
                      $terms = $this->entityHelper->getTermReference($voc, $term);
                      if ($terms) {
                        $nodeArray[$itemKey] = $terms;
                      }
                    }
                  }

                  if ($itemValue['properties']['setting']['target_type'] == 'node') {
                    $title = $data[$itemValue['key']];

                    $node = \Drupal::entityQuery('node')
                      ->condition('type', 'gaceta')
                      ->condition('title', $title, 'IN')
                      ->execute();
                    if ($node) {
                      $nodeArray[$itemKey][] = ['target_id' =>  key($node)];
                    }
                  }
                  break;

                case 'datetime':
                  $dataArrayItems = explode('|', $data[$itemValue['key']]);
                  foreach ($dataArrayItems as $dataArrayItem) {
                    $dateArray = explode(':', $dataArrayItem);
                    if (count($dateArray) > 1) {
                      $dateTimeStamp = strtotime($dataArrayItem);
                      $newDateString = date('Y-m-d\TH:i:s', $dateTimeStamp);
                    } else {
                      $dateTimeStamp = strtotime($dataArrayItem);
                      $newDateString = date('Y-m-d', $dateTimeStamp);
                    }
                    $nodeArray[$itemKey][] = ["value" => $newDateString];
                  }
                  break;

                case 'text_long':
                  $summary = $data[$itemValue['key']];
                  $nodeArray[$itemKey] = [
                    'value' => $summary,
                    'format' => 'full_html'
                  ];
                  break;

                case 'text_with_summary':
                  if ($itemKey == 'body') {

                    $value = $summary = "";

                    if (isset($itemValue['summary']) && isset($itemValue['summary']['key'])) {
                      $summary = $data[$itemValue['summary']['key']];
                    }

                    if (isset($itemValue['value']) && isset($itemValue['value']['key'])) {
                      $value = $data[$itemValue['value']['key']];
                    }

                    $nodeArray['body'] = [
                        'value' => $value,
                        'summary' => $summary,
                        'format' => 'full_html',
                    ];
                  }
                  break;

                case 'integer':
                  if ($itemKey != 'nid') {
                    $nodeArray[$itemKey] = $data[$itemValue['key']];
                  }
                  break;

                case 'created':
                  break;

                case 'string':
                  $nodeArray[$itemKey] = Html::escape($data[$itemValue['key']]);
                  break;

                case 'list_string':
                  $nodeArray[$itemKey] = $data[$itemValue['key']];
                  break;

                case 'entity_reference_revisions':
                  /* TODO pendiente */
                  break;

                default:
                  echo "default $type : $itemKey " . $data[$itemValue['key']];
                  echo "<br>";
                  break;
              }
            }
          }

          $nodeArray['type'] = $contentTypes;

          $nodes[] = $nodeArray;

          $node = Node::create($nodeArray);
          $node->save();
          unset($node);
          /*kint($nodeArray);
          exit(0);*/

          $nodeArray = [];
          sleep(1);
        }

        file_delete($uploadZipFile->id());
        file_delete($uploadCsvFile->id());

        drupal_set_message("Importado exitosamente.");

        /**$result = 0;
        while ($data  = $this->entityHelper->getCsvData($handle)) {
          $rowData = [
            'data' => $data,
            'key_index' => $keyIndex,
            'content_type' => $contentTypes,
            'fields' => $fields,
            'result' => ++$result
          ];

          $operations[] = [
            '\Drupal\senapi_export_import\EntitySaveHelper::importFromCsv',
            [$rowData],
          ];
        }

        $this->batchOprations($operations, $handle);

        file_delete($uploadZipFile->id());
        file_delete($uploadCsvFile->id());*/
        break;

      case 'import_old':
        $dirPrensa = $this->fileSystem->realpath( "public://") . "/senapi/prensa/" . date("Y-m");
        if (!is_dir($dirPrensa)) {
          mkdir($dirPrensa, 0777, TRUE);
          chmod($dirPrensa, 0777);
        }

        $newNode = $form_state->getValue('new_node');
        $exportCreated = $form_state->getValue('export_created');
        $result = 0;
        $uploadZip = $form_state->getValue('upload_zip');
        $uploadZipFile = $this->entityTypeManager->getStorage('file')->load($uploadZip[0]);
        $archiver = $this->entityHelper->getArchiver($this->fileSystem->realpath($uploadZipFile->getFileUri()));

        $resultData = $archiver->extractTo($this->entityHelper->folders['import_media']['relative']);
        if ($resultData === TRUE) {
          $uploadCsv = $form_state->getValue('upload_csv');
          $uploadCsvFile = $this->entityTypeManager->getStorage('file')->load($uploadCsv[0]);
          $handle = $this->entityHelper->getFileHandler($uploadCsvFile->getFileUri());

          $firstRow = $this->entityHelper->getCsvData($handle);

          $colummsIndex = [
            'nid' => key(preg_grep("/^.+nid$/i", $firstRow)),
            'title' => key(preg_grep("/^.+title$/i", $firstRow)),
            'body_summary' => key(preg_grep("/^.+body_summary$/i", $firstRow)),
            'body_description' => key(preg_grep("/^.+body_description$/i", $firstRow)),
            'created' => key(preg_grep("/^.+created$/i", $firstRow)),
            'img_src' => key(preg_grep("/^.+image$/i", $firstRow)),
            'img_alt' => key(preg_grep("/^.+image_alt$/i", $firstRow)),
            'img_title' => key(preg_grep("/^.+image_title$/i", $firstRow)),
            'field_region' => key(preg_grep("/^.+region$/i", $firstRow)),
            'field_tags' => key(preg_grep("/^.+tags$/i", $firstRow)),
          ];

          $operations = [];
          $importMedia = $this->entityHelper->folders['import_media']['absolute'];
          while (($data = $this->entityHelper->getCsvData($handle)) !== FALSE) {
            $rowData = [
              'data' => $data,
              'columns' => $colummsIndex,
              'content_type' => $contentTypes,
              'new_node' => $newNode,
              'fields' => $fields,
              'save_body' => $exportBody,
              'save_created' => $exportCreated,
              'result' => ++$result,
              'import_media' => $importMedia
            ];

            $operations[] = [
              '\Drupal\senapi_export_import\EntitySaveHelper::importImageFromCsv',
              [$rowData],
            ];
          }

          $this->batchOprations($operations, $handle);

          file_delete($uploadZipFile->id());
          file_delete($uploadCsvFile->id());
        } else {
          drupal_set_message($this->t('Hay algún problema relacionado con la extracción
          del archivo. Cargue y vuelva a intentarlo.'), 'error', FALSE);
        }
        break;

      case 'export_menu':
        $fields = $form_state->getValue('menu_types');
        $filenameData =   'menu_' . join("_", $fields) . '_' . $current . '.csv';

        $pathData = $this->entityHelper->createCsvFileExportMenuData($filenameData, $fields);

        $urlData = file_create_url($pathData);

        $response = 'Descargar <a href="' . $urlData . '" target="_blank">' . $filenameData . '</a> <br>';
        drupal_set_message(t($response));
        break;

      case 'import_menu':
        $uploadCsv = $form_state->getValue('upload_csv');
        $uploadCsvFile = $this->entityTypeManager->getStorage('file')->load($uploadCsv[0]);
        $handle = $this->entityHelper->getFileHandler($uploadCsvFile->getFileUri());

        $firstRow = $this->entityHelper->getCsvData($handle);

        $colummsKey = [
          'id' => key(preg_grep("/^id$/i", $firstRow)),
          'uuid' => key(preg_grep("/^uuid$/i", $firstRow)),
          'langcode' => key(preg_grep("/^langcode$/i", $firstRow)),
          'bundle' => key(preg_grep("/^bundle$/i", $firstRow)),
          'enabled' => key(preg_grep("/^enabled$/i", $firstRow)),
          'title' => key(preg_grep("/^title$/i", $firstRow)),
          'description' => key(preg_grep("/^description$/i", $firstRow)),
          'menu_name' => key(preg_grep("/^menu_name$/i", $firstRow)),
          'link' => key(preg_grep("/^link$/i", $firstRow)),
          'external' => key(preg_grep("/^external$/i", $firstRow)),
          'rediscover' => key(preg_grep("/^rediscover$/i", $firstRow)),
          'weight' => key(preg_grep("/^weight$/i", $firstRow)),
          'expanded' => key(preg_grep("/^expanded$/i", $firstRow)),
          'parent' => key(preg_grep("/^parent$/i", $firstRow)),
          'changed' => key(preg_grep("/^changed$/i", $firstRow)),
          'default_langcode' => key(preg_grep("/^default_langcode$/i", $firstRow)),
        ];
        $result = 0;
        $operations = [];
        while (($data = $this->entityHelper->getCsvData($handle)) !== FALSE) {
          $rowData = [
            'data' => $data,
            'columns' => $colummsKey,
            'result' => ++$result,
          ];

          $operations[] = [
            '\Drupal\senapi_export_import\EntitySaveHelper::importMenuFromCsv',
            [$rowData],
          ];
        }

        $this->batchOprations($operations, $handle);
        file_delete($uploadCsvFile->id());
        break;
    }
  }

  /**
   * Ejecución de la operación por lotes.
   *
   *
   * @param mixed $operations
   *   Object contains all information related to batch.
   * @param mixed $handle
   *   Manage batch operation.
   */
  public function batchOprations($operations, $handle) {
    if (count($operations)) {
      $batch = [
        'title' => $this->t('Importando CSV...'),
        'operations' => $operations,
        'finished' => '\Drupal\senapi_export_import\EntitySaveHelper::importFromCsvFinishedCallback',
        'error_message' => $this->t('La importación ha encontrado un error.'),
        'progress_message' => $this->t('Importado @current de @total fila.'),
      ];
      batch_set($batch);
      fclose($handle);
    }
  }
}